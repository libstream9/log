#ifndef STREAM9_LOG_GRABBER_HPP
#define STREAM9_LOG_GRABBER_HPP

#include <stream9/source_location.hpp>

#include <string>
#include <string_view>
#include <vector>

namespace stream9::log {

class grabber
{
public:
    grabber();
    ~grabber() noexcept;

    // query
    auto const& debug() const { return m_debug; }
    auto const& info() const { return m_info; }
    auto const& warning() const { return m_warning; }
    auto const& error() const { return m_error; }

    bool operator()(priority, source_location const&,
                    std::string_view category, std::string_view message) noexcept;

private:
    handler_t m_old_handler;
    std::vector<std::string> m_debug;
    std::vector<std::string> m_info;
    std::vector<std::string> m_warning;
    std::vector<std::string> m_error;
};

} // namespace stream9::log

#endif // STREAM9_LOG_GRABBER_HPP
