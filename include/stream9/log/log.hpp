#ifndef STREAM9_LOG_LOG_HPP
#define STREAM9_LOG_LOG_HPP

#include <stream9/source_location.hpp>

#include <compare>
#include <functional>
#include <sstream>
#include <string_view>

namespace stream9::log {

class stream;

stream err(source_location loc = {});
stream warn(source_location loc = {});
stream dbg(source_location loc = {});
stream info(source_location loc = {});

enum class priority { debug, info, warning, error };

priority level() noexcept;
void set_level(priority) noexcept;

using handler_t = std::function<
    bool(priority, source_location const&,
         std::string_view category, std::string_view message) >;

handler_t set_handler(handler_t) noexcept;

template<typename T>
concept iomanipulator = std::invocable<T, stream&>
                     || std::invocable<T, std::ostream&>;

class stream
{
public:
    stream(priority, source_location loc = {});

    ~stream() noexcept;

    // modifier
    void set_space(bool const v) noexcept
    {
        m_nospace = !v;
    }

    void set_cr(bool const v) noexcept
    {
        m_nocr = !v;
    }

    // operator
    template<typename T>
        requires (!iomanipulator<T>)
    stream&& operator<<(T&& v) &&
    {
        if (level() <= m_priority) {
            print(v);
        }

        return std::move(*this);
    }

    template<typename T>
        requires (!iomanipulator<T>)
    stream& operator<<(T const& v) &
    {
        if (level() <= m_priority) {
            print(v);
        }

        return *this;
    }

    template<typename Fn>
    stream&&
    operator<<(Fn&& fn) &&
        requires requires (Fn f, stream&& s) { { f(s) } -> std::same_as<void>; }
    {
        if (level() <= m_priority) {
            fn(*this);
        }

        return std::move(*this);
    }

    template<typename Fn>
    stream&
    operator<<(Fn&& fn) &
        requires requires (Fn f, stream& s) { { f(s) } -> std::same_as<void>; }
    {
        if (level() <= m_priority) {
            fn(*this);
        }

        return *this;
    }

    stream&& operator<<(std::ostream& (*func)(std::ostream&)) &&
    {
        if (level() <= m_priority) {
            func(m_stream);
        }

        return std::move(*this);
    }

    stream& operator<<(std::ostream& (*func)(std::ostream&)) &
    {
        if (level() <= m_priority) {
            func(m_stream);
        }

        return *this;
    }

    // conversion
    operator std::ostream& () noexcept
    {
        return m_stream;
    }

private:
    template<typename T>
    void print(T const& v)
    {
        if (m_first) {
            m_first = false;
        }
        else if (!m_nospace) {
            m_stream.put(' ');
        }

        m_stream << v;
    }

private:
    std::ostringstream m_stream;
    source_location m_loc;
    priority m_priority;
    bool m_first : 1 = true;
    bool m_nospace : 1 = false;
    bool m_nocr : 1 = false;
};

// manipulators
inline void
space(stream& s) noexcept
{
    s.set_space(true);
}

inline void
nospace(stream& s) noexcept
{
    s.set_space(false);
}

inline void
cr(stream& s) noexcept
{
    s.set_cr(true);
}

inline void
nocr(stream& s) noexcept
{
    s.set_cr(false);
}

inline std::strong_ordering
operator<=>(priority x, priority y)
{
    return static_cast<int>(x) <=> static_cast<int>(y);
}

} // namespace stream9::log

#endif // STREAM9_LOG_LOG_HPP
