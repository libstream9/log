#include <stream9/log/log.hpp>
#include <stream9/log/grabber.hpp>

#include <stream9/cstring_ptr.hpp>

#include <iostream>

#include <systemd/sd-journal.h>

namespace stream9::log {

static int
to_journald_priority(priority p) noexcept
{
    switch (p) {
        using enum priority;
        case debug:
            return LOG_DEBUG;
        case info:
            return LOG_INFO;
        case warning:
            return LOG_WARNING;
        default:
        case error:
            return LOG_ERR;
    }
}

static bool
journald_handler(priority pri,
                 source_location const& loc,
                 std::string_view /*category*/,
                 std::string_view message) noexcept
{
    cstring_ptr msg { message };
    auto rc = ::sd_journal_send(
        "MESSAGE=%s", msg.c_str(),
        "PRIORITY=%i", to_journald_priority(pri),
        "CODE_FUNC=%s", loc.function_name(),
        "CODE_LINE=%d", loc.line(),
        "CODE_FILE=%s", loc.file_name(),
        nullptr
    );
    return rc == 0;
}

static bool
console_handler(priority pri,
                source_location const& loc,
                std::string_view /*category*/,
                std::string_view message) noexcept
{
    switch (pri) {
        case priority::debug: {
            std::ostringstream ss;
            ss << loc.file_name() << ":" << loc.line() << ": " << message;

            std::cerr << ss.view();
            break;
        }
        case priority::info:
        case priority::warning:
        case priority::error:
            std::cerr << message;
            break;
    }

    return !std::cerr.fail();
}

static bool
default_handler(priority pri, source_location const& loc,
    std::string_view cat, std::string_view message) noexcept
{
    static bool has_console = true;

    if (has_console) {
        if (console_handler(pri, loc, cat, message)) {
            return true;
        }
        else {
            has_console = false;
        }
    }

    return journald_handler(pri, loc, cat, message);
}

handler_t g_handler { default_handler };

handler_t
set_handler(handler_t h) noexcept
{
    auto old = std::move(g_handler);

    g_handler = std::move(h);

    return old;
}

static void
submit_log(priority p, source_location const& loc,
           std::string_view category, std::string_view message)
{
    g_handler(p, loc, category, message);
}

/*
 * stream
 */
stream::
stream(priority const p,
       source_location loc/*= {}*/)
    : m_loc { std::move(loc) }
    , m_priority { p }
{}

stream::
~stream() noexcept
{
    if (level() <= m_priority) {
        if (!m_nocr) {
            m_stream << '\n';
        }
        submit_log(m_priority, m_loc, {}, m_stream.view());
    }
}

stream
dbg(source_location loc/*= {}*/)
{
    return { priority::debug, std::move(loc) };
}

stream
info(source_location loc/*= {}*/)
{
    return { priority::info, std::move(loc) };
}

stream
warn(source_location loc/*= {}*/)
{
    return { priority::warning, std::move(loc) };
}

stream
err(source_location loc/*= {}*/)
{
    return { priority::error, std::move(loc) };
}

priority s_lvl = priority::debug;

priority
level() noexcept
{
    return s_lvl;
}

void
set_level(priority lvl) noexcept
{
    s_lvl = lvl;
}

/*
 * grabber
 */
grabber::
grabber()
{
    m_old_handler = set_handler(std::ref(*this));
}

grabber::
~grabber()
{
    set_handler(m_old_handler);
}

bool grabber::
operator()(priority const p, source_location const&,
           [[maybe_unused]] std::string_view const category,
           std::string_view const message) noexcept
{
    switch (p) {
        case priority::debug:
            m_debug.emplace_back(message);
            break;
        case priority::info:
            m_info.emplace_back(message);
            break;
        case priority::warning:
            m_warning.emplace_back(message);
            break;
        case priority::error:
            m_error.emplace_back(message);
            break;
    }

    return true;
}

} // namespace stream9::log
